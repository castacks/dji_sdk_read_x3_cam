##DJI Onboard SDK ROS Package for Video Decoding on Manifold

This video-decoding package is a specifically desiged package for Manifold to read the X3 video stream. It was by  modified the Airlab to allow changing resolution in realtime

### To get images from the Gimbal ###

Install the dependencies:


```
#!bash

sudo apt-get install ros-indigo-image-transport
sudo apt-get install ros-indigo-cv-bridge
sudo apt-get install ros-indigo-image-view  # this is just to see the published images

```


Compile the camera node:


```
#!bash

cd ~/your_ws/
source devel/setup.bash
catkin_make

```
Configure the display and enter the sudo mode by running the following script: 

```
#!bash
./src/dji_sdk_read_x3_cam/scripts/cameraconf.sh

```

Launch the node:


```
#!bash

roslaunch dji_sdk_read_cam manifold_cam.launch

```

IMPORTANT: You will several errors of the form:


```
#!bash

[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
---> TVMR: Video-conferencing detected !!!!!!!!!
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced

```

Those errors are not important. However, you **must** see the line `---> TVMR: Video-conferencing detected !!!!!!!!!`. If you did not get this line, you have a problem with the display. In this case, try to run:


```
#!bash

export DISPLAY=:0.0
```




To see the images, in another terminal run:


```
#!bash

rosrun image_view image_view image:=dji_sdk/image_raw

```

To change the image from gray scale to color, in another terminal run:


```
#!bash

rosparam set /dji_sdk_read_cam/gray_or_rgb 1

```

From color to gray scale:


```
#!bash

rosparam set /dji_sdk_read_cam/gray_or_rgb 0

```


### Who do I talk to? ###

* Guilherme Pereira (gpereira@ufmg.br)

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2015, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.